package jp.co.kenshu;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import jp.co.kenshu.mapping.EmployeeData;

@Component("itemProcessor")
public class EmployeeProcessor implements ItemProcessor<EmployeeData, EmployeeData> {

	// @Override
	public EmployeeData process(EmployeeData paramI) throws Exception {
		EmployeeData data = new EmployeeData();
		data.setId(paramI.getId());
		data.setName(paramI.getName() + "さん");
		data.setNote(paramI.getNote() + "以上です。");
		return data;
	}

}
